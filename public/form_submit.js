$(function () {
    $('.frm1').submit(function () {
        var product_name = $('.product_name').val();
        var quantity = $('.quantity').val();
        var price = $('.price').val();
        var id='';
        var totall= quantity * price;
        $('.product_name_error').html('');

        if(/^[a-zA-Z0-9- ]*$/.test(product_name) == false) {
            $('.product_name_error').html('Product Name should be in Alphabets.');
        }
        else{
            if($('.id').val()){
                id = $('.id').val();
            }
            $.ajax({
                type:'post',
                url:'api/save_data',
                data:{product_name:product_name,quantity:quantity,price:price,total:totall,id:id},
                dataType:'json',
                success:function (data) {
                    if(data == 1){
                        $('.final_rows').html('');
                        var append_data=[];
                        $.getJSON( "data.json", function( ans ) {
                            $.each( ans, function( key, val ) {
                                $('.final_rows').append('<tr><td>'+(key+1)+'</td><td class="pnm'+key+'">'+val.product_name+'</td><td class="q'+key+'">'+val.quantity+'</td><td class="pr'+key+'">'+val.price+'</td><td>'+val.total+'</td><td><button type="button" class="btn btn-outline-primary" onclick="edit_data('+key+');">Edit</button></td></tr>');
                            });
                        });
                    }
                }

            });
        }
        $(this)[0].reset();
        $('.id').val('');
    });

});
function edit_data(id) {
    alert(id);
    $('.product_name').val($('.pnm'+id).text());
    $('.quantity').val($('.q'+id).text());
    $('.price').val($('.pr'+id).text());
    $('.id').val(id);
}

