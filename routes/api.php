<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('save_data',function (Request $request){
    $data = [];
    //print_r($request->all()); exit();
    $data['product_name'] = $request->product_name;
    $data['quantity'] = $request->quantity;
    $data['price'] = $request->price;
    $data['total'] = $request->total;
    $data1=[$data];$rr='';
    if(file_exists(public_path().'/data.json')){
        $inp = file_get_contents(public_path().'/data.json');
        $tempArray = json_decode($inp);
        if(!empty($request->id)){
            foreach ($tempArray as $key => $entry) {
                if($key==$request->id){
                    $tempArray[$key]=$data;
                }
            }
            $rr = $tempArray;
            //print_r($tempArray); exit();
            //exit();
        }
        else{
            $rr = array_merge($data1,$tempArray);
        }


        $jsonData = json_encode($rr);
        if(file_put_contents(public_path().'/data.json', $jsonData)){
            chmod(public_path().'/data.json', 0777);
            return 1;
        }
        else{
            return 0;
        }

    }else{
        $fp = fopen(public_path().'/data.json', 'a+');
        if(fwrite($fp, json_encode($data1))){
            fclose($fp);
            chmod(public_path().'/data.json', 0777);
            return 1;
        }
        else{
            return 0;
        }

    }
});