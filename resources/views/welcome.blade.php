<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Example of Bootstrap 3 Vertical Form Layout</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/dist/css/bootstrap.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('bootstrap/dist/js/bootstrap.js') }}"></script>
    <script src="{{ asset('form_submit.js') }}"></script>
    <style type="text/css">
        .bs-example {
            margin: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-4" style="padding: 2%;">
                <div class="card" style="padding: 2%;">
                    <div class="bs-example ">
                        <h4 class="card-title">Add Product</h4>
                        <form action="" class="frm1" method="post" autocomplete="off" onsubmit="return false;">
                            <div class="form-group">
                                <label>Product Name</label>
                                <input type="hidden" class="id" value="">
                                <input type="text" class="form-control product_name" placeholder="Enter Product Name"
                                       required>
                                <span class="text-danger product_name_error"></span>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input type="number" class="form-control quantity" placeholder="Enter Password" required
                                       min="0">
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input type="number" class="form-control price" placeholder="Enter Price" required
                                       min="0">
                            </div>
                            <button type="submit" class="btn btn-primary add_data">Submit</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-2">

            </div>
            <div class="col-md-6" style="padding-top: 3%;">
                <h4>List of Products</h4>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Product Name</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Price</th>
                        <th scope="col">Total Amount</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody class="final_rows">
                    @php
                        if(file_exists(public_path().'/data.json')){
                        $items = file_get_contents(public_path().'/data.json');
                            $tempArray = json_decode($items);
                        if(sizeof($tempArray) > 0){
                        $count=1;
                            foreach ($tempArray as $item) {

                    echo '<tr>
                        <td>'.$count.'</td>
                        <td class="pnm'.($count-1).'">'.$item->product_name.'</td>
                        <td class="q'.($count-1).'">'.$item->quantity.'</td>
                        <td class="pr'.($count-1).'">'.$item->price.'</td>
                        <td>'.$item->total.'</td>
                        <td><button type="button" class="btn btn-outline-primary" onclick="edit_data('.($count-1).');">Edit</button></td>
                    </tr>';

                    $count++;
                             }
                        }
                        }

                    @endphp
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>

</body>
</html>